<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Model::index');

$routes->get('pages', 'Pages::index', ["filter" => "basicauth"]);
$routes->get('pages-en', 'Pages::index/en', ["filter" => "basicauth"]);
$routes->get('pages/edit/(:num)', 'Pages::edit/$1', ["filter" => "basicauth"]);
$routes->get('pages/index', 'Pages::index', ["filter" => "basicauth"]);
$routes->get('pages/images', 'Pages::images', ["filter" => "basicauth"]);
$routes->get('pages/index/en', 'Pages::index/en', ["filter" => "basicauth"]);


$routes->get('models', 'Model::index');
$routes->get('models-en', 'Model::index/en');
// $routes->get('models/(:any)', 'Model::index/$1');

$routes->get('models/(:segment)', 'Model::view/$1');
$routes->get('models/(:segment)/classes', 'Model::classes/$1');
$routes->get('models/(:segment)/classes-en', 'Model::classes/$1/en ');
$routes->get('models/(:segment)/(:any)', 'Model::view/$1/$2');

$routes->match(['get', 'post'], 'models/(:segment)/upload', 'Model::upload/$1');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
