<h1><?= getSubsection($content,"contact::title") ?></h1>

<p>
    <?= anchor('/site/page/contact/'.($language=='en' ? 'nl' : 'en'),'switch ' . ($language=='en' ? 'naar Nederlands' : 'to English')) ?>
</p>

<p>
    <?= getSubsection($content,"contact::body") ?>
</p>

