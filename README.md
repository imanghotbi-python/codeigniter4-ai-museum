# AI Museum Analysis

## Development

### Steps to reproduce (temporary)

1. Install via composer
2. Move the public and vendor directory
3. In public/index.php, change line 32 to "require FCPATH . '../codeigniter4/app/Config/Paths.php';"
4. In codeigniter4/app/Config/Paths.php change line 28 to "public $systemDirectory = **DIR** . '/../../../vendor/codeigniter4/framework/system';"
5. Copy app/Controllers/.. Model.php, Pages.php and Site.php Home.php files
6. Copy app/Views/.. model pages site template folders
7. Copy config folder
8. Copy public/.. assets css js folders
9. Copy public/favicon.ico
10. Update app/Config/Filters.php (with mainly BasicAuth)
11. Update app/Config/Mimes.php (file associations?)
12. Update app/Config/Routes.php (Site routing) [AutoRouting is dangerous, but is turned on] --> Use Auto Routing (Improved) 
14. Copy app/Config/Custom.php
15. In app/Config/Database.php update entries => hostname=database, usrname, password and database = develop
16. Copy app/Models.. ContentModel.php EnrichmentsModel.php MaskerModel.php ModelsModel.php SectionsModel.php UserModel.php
17. Copy app/Helper/toolbox_helper.php
18. Copy app/Filters/BasicauthFilter.php 
 
### How to set up

Follow the steps below to set up the development environment. Make sure port 80 & 443 are available for the containers, so stop other web servers before you start this up.

### Required software

1. PHP (preferably same major.minor version as the php-fpm docker container, see docker/php-fpm/Dockerfile)
2. Composer ([latest](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos))
3. docker [latest](https://docs.docker.com/engine/install/)
4. docker-compose [latest](https://docs.docker.com/compose/install/)
5. nodejs ([latest](https://nodejs.org/en/download/package-manager/) or with [nvm](https://github.com/nvm-sh/nvm))

### Development set up

<!--1. Run `composer install` in the project directory (or with `--ignore-platform-reqs` if you're missing some php extensions)-->
2. Run `docker login registry.gitlab.com` (see [GitLab documentation](https://docs.gitlab.com/ee/user/packages/container_registry/index.html#authenticating-to-the-gitlab-container-registry))
3. Run `docker-compose up` in a different cli tab (to see the log output right away)
4. Run `chmod 777 data/files` to make the files share writable (use sudo when you get a permission error)
5. Add `127.0.0.1 aimuseum.dryrun.link` to your hosts file (/etc/hosts)
6. Open [aimuseum.dryrun.link](aimuseum.dryrun.link) in your favorite browser

If everything is setup correctly you should see something like this when running `docker ps`:
```
❯ docker ps
CONTAINER ID   IMAGE                                                                              COMMAND                  CREATED        STATUS                  PORTS                                                                      NAMES
ec0cf447d789   registry.gitlab.com/naturalis/bii/ai-museum/docker-ai-museum/traefik:latest        "/entrypoint.sh --gl..."   18 hours ago   Up 18 hours             0.0.0.0:80->80/tcp, :::80->80/tcp, 0.0.0.0:443->443/tcp, :::443->443/tcp   codeigniter4-ai-museum_traefik_1
066600e010c4   registry.gitlab.com/naturalis/bii/ai-museum/docker-ai-museum/nginx:latest          "/docker-entrypoint...."   18 hours ago   Up 18 hours             80/tcp                                                                     codeigniter4-ai-museum_nginx_1
4d0b8aa1e909   registry.gitlab.com/naturalis/bii/ai-museum/docker-ai-museum/php-dev:latest        "docker-php-entrypoi..."   18 hours ago   Up 18 hours             9000/tcp                                                                   codeigniter4-ai-museum_php-fpm_1
57f647b1791d   registry.gitlab.com/naturalis/bii/ai-museum/docker-ai-museum/mariadb:latest        "docker-entrypoint.s..."   18 hours ago   Up 18 hours (healthy)                                                                              codeigniter4-ai-museum_database_1
37e9dd01ba10   registry.gitlab.com/naturalis/bii/ai-museum/docker-ai-museum/docker-proxy:latest   "/docker-entrypoint...."   18 hours ago   Up 18 hours                                                                                        codeigniter4-ai-museum_docker-proxy_1

```

### Xdebug set up

1. Add the following to `launch.json` in the `.vscode` folder in the root of the project:
    ```json
    {
        "version": "0.2.0",
        "configurations": [
            {
                "name": "Listen for Xdebug",
                "type": "php",
                "request": "launch",
                "port": 9000,
                "pathMappings": {
                    "/var/www/": "${workspaceRoot}/codeigniter4",
                    "/var/www/public": "${workspaceRoot}/public",
                    "/var/www/vendor": "${workspaceRoot}/vendor"
                },
            }
        ]
    }
    ```
2. In the Debug and Run menu, hit `Listen for Xdebug`.
3. Refresh your browser.